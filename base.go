package redis

import (
	"encoding/json"
	"fmt"
	"github.com/garyburd/redigo/redis"
	"log"
	"reflect"
	"strconv"
	"strings"
)

type RedisClient struct {
	Conn *redis.Pool
	OK   string
}

/**
 *  获取客户端连接
 */
func NewClient(config map[string]interface{}) *RedisClient {
	return &RedisClient{
		Conn: Connect(config),
		OK:   "OK",
	}
}

/**
 *  连接redis
 */
func Connect(config map[string]interface{}) *redis.Pool {
	return &redis.Pool{ //实例化一个连接池
		MaxIdle: 100, //最初的连接数量
		//MaxActive:1000000,    //最大连接数量
		MaxActive:   0,   //连接池最大连接数量,不确定可以用0（0表示自动定义），按需分配
		IdleTimeout: 300, //连接关闭时间 300秒 （300秒不使用自动关闭）
		Dial: func() (redis.Conn, error) { //要连接的redis数据库
			c, er := redis.Dial(
				"tcp",
				ToString(config["host"])+":"+ToString(config["port"]),
				redis.DialDatabase(ToInt(config["db"])),
				redis.DialPassword(ToString(config["pass"])),
			)
			if er == nil {
				return c, nil
			} else {
				return nil, er
			}
		},
	}
}

/**
 *  获取redis数据
 */
func (this *RedisClient) Command(cmd string, keys ...interface{}) (str string, err error) {
	Rds := this.Conn.Get()
	defer Rds.Close()

	switch strings.ToUpper(cmd) {
	case "HMGET":
		res, err := redis.Strings(Rds.Do(cmd, keys...))
		if err == nil {
			return res[0], nil
		}
	case "HKEYS":
		res, err := redis.Strings(Rds.Do(cmd, keys...))
		if err == nil {
			if strs, er := json.Marshal(res); er == nil {
				return string(strs), nil
			}
		}
	case "GET":
		res, err := redis.String(Rds.Do(cmd, keys...))
		if err == nil {
			return res, nil
		}
	case "RPOP":
		res, err := redis.String(Rds.Do(cmd, keys...))
		if err == nil {
			return res, nil
		}

	case "RPOPLPUSH":
		res, err := redis.String(Rds.Do(cmd, keys...))
		if err == nil {
			return res, nil
		}

	case "LREM":
		res, err := redis.String(Rds.Do(cmd, keys...))
		if err == nil {
			return res, nil
		}

	default:

	}

	return str, err
}

/**
 *  获取redis数据
 */
func (this *RedisClient) Del(key string) (bool, error) {
	Rds := this.Conn.Get()
	defer Rds.Close()

	return redis.Bool(Rds.Do("DEL", key))
}

/**
 *  设置时效性
 */
func (this *RedisClient) RedisExpire(key string, expire int) {
	Rds := this.Conn.Get()
	defer Rds.Close()
	_, err := Rds.Do("expire", key, expire)
	if err != nil {
		log.Println(err.Error())
	}
}

/**
 *  转字符串
 */
func ToString(data interface{}) string {
	if data == nil {
		return ""
	}

	switch reflect.TypeOf(data).String() {
	case "string":
		return data.(string)
	case "int":
		return strconv.Itoa(data.(int))
	case "float64":
		return strconv.Itoa(int(int64(data.(float64))))
	case "float32":
		return strconv.Itoa(int(int32(data.(float32))))
	default:
		return fmt.Sprintf("%v", data)
	}
}

/**
 *  转字符串
 */
func ToInt(data interface{}) int {
	if data == nil {
		return 0
	}

	switch reflect.TypeOf(data).String() {
	case "string":
		va, _ := strconv.Atoi(data.(string))
		return va
	case "int":
		return data.(int)
	case "float64":
		return int(int64(data.(float64)))
	case "float32":
		return int(int32(data.(float32)))
	case "int64":
		return int(data.(int64))
	default:
		return 0
	}
}
